import numpy as np
from mayavi import mlab
from kl11_equilibrium_get import equilibrium
from contour_handler import ContourHandler
from emission_tracker_settings import Device
import matplotlib.pyplot as plt

def make_tokamak_from_walls(device='JET'):
    device_walls = Device(device)
    device_walls.wall_r
    r_out = device_walls.wall_r
    z_out = device_walls.wall_z
    dphi = np.pi/250.0
    print(np.shape(r_out))
    r_out_2 = np.vstack((r_out, r_out[::-1]))
    z_out_2 = np.vstack((z_out, z_out[::-1]))
    print(np.shape(r_out_2))
    r_out_3 = np.reshape(r_out_2, (np.shape(r_out_2)[0]*np.shape(r_out_2)[1]))
    z_out_3 = np.reshape(z_out_2, (np.shape(r_out_2)[0]*np.shape(r_out_2)[1]))
    print(np.shape(r_out_3))
    phi = np.linspace(-np.pi+dphi, np.pi/2.+dphi, 1000)
    print(np.shape(phi))
    R, Phi = np.meshgrid(r_out_2, phi)
    Z, Phi = np.meshgrid(z_out_2, phi)
    print(np.shape(R))
    print(R)
    x, y, z = simple_rz_phi_to_xyz(R, Z, Phi)
    s = mlab.mesh(x, y, z, colormap="Greys")
    s = mlab.mesh(x, y, z, colormap="Greys")
    s.actor.property.frontface_culling=True
def make_tokamak_from_equilibrium(device='JET', shot=None, time=None):
    equil = equilibrium(device=device, shot=shot, time=time)
    rmin = 1.5
    rmax = 4.0
    zmin = -1.8
    zmax = 2.02
    contour_obj = ContourHandler(equil, rmin, rmax, zmin, zmax)
    contour_obj.get_contourline_sorted(1.0, add_xpt_bool=True)
    r_out = np.hstack((contour_obj.line_outboard_r, contour_obj.line_inboard_r[::-1]))
    z_out = np.hstack((contour_obj.line_outboard_z, contour_obj.line_inboard_z[::-1]))
    dphi = np.pi/250.0
    print(np.shape(r_out))
    r_out_2 = np.vstack((r_out, r_out[::-1]))
    z_out_2 = np.vstack((z_out, z_out[::-1]))
    print(np.shape(r_out_2))
    r_out_3 = np.reshape(r_out_2, (np.shape(r_out_2)[0]*np.shape(r_out_2)[1]))
    z_out_3 = np.reshape(z_out_2, (np.shape(r_out_2)[0]*np.shape(r_out_2)[1]))
    print(np.shape(r_out_3))
    phi = np.linspace(-np.pi+dphi, np.pi/3.+dphi, 1000)
    print(np.shape(phi))
    R, Phi = np.meshgrid(r_out_2, phi)
    Z, Phi = np.meshgrid(z_out_2, phi)
    print(np.shape(R))
    print(R)
    x, y, z = simple_rz_phi_to_xyz(R, Z, Phi)
    s1 = mlab.mesh(x, y, z, colormap="Wistia")


def make_tokamak(R0, a):
    dr, dtheta, dphi  = 0.1, np.pi/250.0, np.pi/250.0
    r = a

    [theta, phi] = np.mgrid[-np.pi:np.pi+dtheta:dtheta,-np.pi:np.pi+dphi:dphi]
    R = R0 + r*np.cos(theta)
    x = R*np.cos(phi)
    y = R*np.sin(phi)
    z = r*np.sin(theta)
    s = mlab.mesh(x, y, z)

def make_open_tokamak(R0, a):
    dr, dtheta, dphi  = 0.1, np.pi/250.0, np.pi/250.0
    r = a
    
    [theta, phi] = np.mgrid[-np.pi:np.pi+dtheta:dtheta,-np.pi:np.pi/2.+dphi:dphi]
    x, y, z = simple_tor_r_theta_phi_to_xyz(r, theta, phi, R0)
    print(np.shape(x))
    print(np.shape(y))
    print(np.shape(z))
    s = mlab.mesh(x, y, z, opacity=1.0)

def plot_current_direction_lines(R0, a):
    # x, y, z are start positions
    x, y, z = np.mgrid[-2:3, -2:3, -2:3]

    # u, v, w are vectors
    r, theta, phi = simple_tor_xyz_to_r_theta_phi(x, y, z, R0)
    dtheta = np.pi/10.
    theta = theta + dtheta
    x1, y1, z1 = simple_tor_r_theta_phi_to_xyz(r, theta, phi, R0)
    u = x1 - x
    v = y1 - y
    w = z1 - z
    obj = mlab.quiver3d(x, y, z, u, v, w, line_width=3, scale_factor=1)

def plot_field_lines(R0, a):
    # x, y, z are start positions
    x, y, z = np.mgrid[-(R0+a):(R0+a):(R0+a)/7., -(R0+a):(R0+a):(R0+a)/7., -(R0+a):(R0+a):(R0+a)/15.]
    r, theta, phi = simple_tor_xyz_to_r_theta_phi(x, y, z, R0)
    where = np.where(np.logical_and(r<=a, r>=-1*a))
    r = r[where]
    theta = theta[where]
    phi = phi[where]
    x, y, z = simple_tor_r_theta_phi_to_xyz(r, theta, phi, R0)

    # u, v, w are vectors
    r, theta, phi = simple_tor_xyz_to_r_theta_phi(x, y, z, R0)
    dphi = np.pi/10.
    phi = phi + dphi
    x1, y1, z1 = simple_tor_r_theta_phi_to_xyz(r, theta, phi, R0)
    u = x1 - x
    v = y1 - y
    w = z1 - z
    obj = mlab.quiver3d(x, y, z, u, v, w, line_width=3, scale_factor=1)

def simple_tor_r_theta_phi_to_xyz(r, theta, phi, R0):
    R = R0 + r*np.cos(theta)
    x = R*np.cos(phi)
    y = R*np.sin(phi)
    z = r*np.sin(theta)
    return x, y, z

def simple_rz_phi_to_xyz(r, z, phi):
    x = r*np.cos(phi)
    y = r*np.sin(phi)
    z = z
    return x, y, z

def simple_tor_xyz_to_r_theta_phi(x, y, z, R0):
    R = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    theta = np.arctan(z/(R-R0))
    r = z/np.sin(theta)
    return r, theta, phi

if __name__ == '__main__':
    R0 = 4.
    a = 1.

    make_tokamak_from_walls(device='JET')
    make_tokamak_from_equilibrium(device='JET', shot=89241, time=50.0)
    #make_open_tokamak(R0, a)
    #plot_field_lines(R0, a)
    # View it.
    mlab.show()
